## Sars-Cov-2 Genome Analysis (Exercise 7)
###
To Do:

* assembly (fasta file for each sample)
* phylogeny of the sample (newick format + graphic)
* regions of high (low) variability (plot for every position + text file for sliding window)

### Input data

* several paired end read Illumina read files in fastq format (one pair per sample)

### Required tools

#### preprocessing and quality checking
* fastqc and trimmomatic (decontamination)
* (can be adopted from Exercise 6)

#### Assembly or Mapping

* DNA can be contaminated with host DNA, bacterial DNA or other sources (which probably influences de novo assembly negatively)
* also then having multiple contigs (e.g. due to uncovered regions) of the Sars-Cov-2 genome is complicating the MSA creation
* better: use bowtie2 to map the reads to a reference and use samtools/bcftools to get the consensus sequence
* if region is uncovered in the mapping: fill with reference sequence, fill with N's or just ignore?


#### Phylogeny

* compute MSA using ClustalO (suitable for small/medium genomes (here: 30kb genome))
* iq-tree on the MSA for ML phylogenetic tree construction, output newick file and plot tree


#### Variablity

The MSA is to be processed by our own script.
The script should work as follows:
Read the MSA and represent it as a 2d array MSA.
The array MSA contains (possibly an integer encoding of) the 
different characters: "A,C,G,T,\_"
In case there are also "N, A, F" etc. what should we do?
This array is $s \times n$, where $s$ is the number of sequences,
$n$ the number of positions in the MSA.

Then create the frequencies array F[0:5,0:n].
So F[0,j] is the frequency of 'A' in column j of MSA,
F[5,j] is the frequency of '-'.
If we have an 'N', then distribute its frequency evenly to the
'A,C,G,T', if it's an 'F' distribute it to the two nucleotides it
represents. If we have an 'I' or 'U' either ignore them or add
dedicated row for these freqencies.

To calculate the variability of the column range [i,j]: we
calculculate the avergage entropy: meaning clalculate the entropy of
each column i, i+1, ... j. then get the mean entropy.
