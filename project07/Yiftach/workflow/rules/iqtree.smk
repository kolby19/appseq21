rule iqtree:
    input:
        aln = "../results/consensus/all.aln.fa",
    output:
        tree = "../results/consensus/all.aln.fa.treefile"
    conda:
        "../envs/bcf.yaml"
    threads:
        4
    shell:
        "iqtree -s {input.aln} -T {threads}"

rule plot_tree:
    input:
        tree = "../results/consensus/all.aln.fa.treefile"
    output:
        pdf = "../results/consensus/final_tree.pdf"
    conda:
        "../envs/bcf.yaml"
    shell:
        "figtree -graphic PDF {input.tree} {output.pdf}"

rule entropy:
    input:
        aln = "../results/consensus/all.aln.fa",
    output:
        entropy = "../results/consensus/entropy.csv",
    conda:
        "../envs/env2.yaml",
    params:
        win = "100",
    shell:
        "python scripts/alignment_cracker.py main {input.aln} {params.win} {output.entropy}"

rule entropyPlot:
    input:
        entropy = "../results/consensus/entropy.csv",
    output:
        plot = "../results/consensus/entropy.png",
    conda:
        "../envs/env2.yaml",
    shell:
        "python scripts/alignment_cracker.py plotFromFile {input.entropy} {output.plot}"


