rule bowtie2_indexing:
    input:
        ref = config["reference"]
    output:
        index = multiext("../results/index/ref_idx", ".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
    log:
        "../logs/sam/bowtie_indexing.log"
    params:
        index_name = "../results/index/ref_idx",
    shell:
        "bowtie2-build -f {input.ref} {params.index_name} > {log} 2>&1"


rule bowtie_mapping:
    input:
        fastq_1 = "../results/qc/trimmed_reads/{sample}_1.fastq.gz",
        fastq_2 = "../results/qc/trimmed_reads/{sample}_2.fastq.gz",
        index = multiext("../results/index/ref_idx", ".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
    output:
        sam = "../results/sam/{sample}.sam"
    log:
        "../logs/sam/{sample}_bowtie_mapping.log"
    params:
        index_name = "../results/index/ref_idx"
    threads:
        4
    conda:
        "../envs/env.yaml"
    shell:
        "bowtie2 -p {threads} -x {params.index_name} -1 {input.fastq_1} -2 {input.fastq_2} -S {output.sam} > {log} 2>&1"