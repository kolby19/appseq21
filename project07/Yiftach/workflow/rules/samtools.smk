rule sam_to_bam:
    input:
        sam = "../results/sam/{sample}.sam"
    output:
        bam = "../results/bam/{sample}.bam"
    log:
        "../logs/bam/{sample}_samtools_view.log"
    threads:
        4
    conda:
        "../envs/env.yaml"    
    shell:
        "samtools view -@ {threads} -S -b {input.sam} > {output.bam} 2> {log} 2>&1"


rule sort_bam:
    input:
        bam = "../results/bam/{sample}.bam"
    output:
        bam_sorted = "../results/bam_sorted/{sample}.bam"
    log:
        "../logs/bam_sorted/{sample}_samtools_sort.log"
    threads:
        4
    conda:
        "../envs/env.yaml"   
    shell:
        "samtools sort -@ {threads} {input.bam} -o {output.bam_sorted} 2> {log} 2>&1"


rule index_bam:
    input:
        bam_sorted = "../results/bam_sorted/{sample}.bam"
    output:
        bam_indexed = "../results/bam_sorted/{sample}.bam.bai"
    log:
        "../logs/bam_sorted/{sample}_samtools_index.log"
    threads:
        4
    conda:
        "../envs/env.yaml"    
    shell:
        "samtools index -@ {threads} {input.bam_sorted} > {output.bam_indexed} 2> {log} 2>&1"


rule bam_stats:
    input:
        bam_sorted = "../results/bam_sorted/{sample}.bam",
        bam_indexed = "../results/bam_sorted/{sample}.bam.bai"
    output:
        stats = "../results/stats/{sample}.txt"
    log:
        "../logs/stats/{sample}_samtools_idxstats.log"
    threads:
        4
    conda:
        "../envs/env.yaml"    
    shell:
        "samtools idxstats -@ {threads} {input.bam_sorted} > {output.stats} 2> {log} 2>&1"


rule average_depth:
    input:
        stats = "../results/stats/{sample}.txt"
    output:
        augmented_stats = "../results/augmented_stats/{sample}.txt"
    script:
        "../scripts/average_depth.py"


rule bam_to_vcf:
    input:
        bam_sorted = "../results/bam_sorted/{sample}.bam",
        bam_indexed = "../results/bam_sorted/{sample}.bam.bai"
    output:
        vcf = "../results/consensus/{sample}.vcf.gz",
    params:
        ref = config["reference"]
    log:
        "../logs/consensus/{sample}_vcf.log"
    threads:
        4
    conda:
        "../envs/bcf.yaml"    
    shell:
        "samtools mpileup -uf {params.ref} {input.bam_sorted} | bcftools call -mv -Oz -o {output.vcf}"


rule index_vcf:
    input:
        vcf = "../results/consensus/{sample}.vcf.gz",
    output:
        tbi = "../results/consensus/{sample}.vcf.gz.tbi",
    log:
        "../logs/consensus/{sample}_index.log"
    threads:
        4
    conda:
        "../envs/bcf.yaml"    
    shell:
        "tabix {input.vcf}"


rule bam_to_fasta:
    input:
        vcf = "../results/consensus/{sample}.vcf.gz",
        tbi = "../results/consensus/{sample}.vcf.gz.tbi"
    output:
        fa = "../results/consensus/{sample}.fa",
		#fa = "../results/consensus/{sample}.fa",
    params:
        ref = config["reference"],
        header = "{sample}"
    log:
        "../logs/consensus/{sample}_fa.log"
    threads:
        4
    conda:
        "../envs/bcf.yaml"    
    shell:
        "cat {params.ref} | bcftools consensus {input.vcf}| python scripts/rehead.py {params.header} > {output.fa}"

rule merge_fastas:
    input:
        fa = "../results/consensus/{sample}.fa",
    output:
        report_flag = touch("../results/temp/{sample}.merge.flag")
    params:
        fas = "../results/consensus/all.fa",
        #fas = "../results/temp/all.fa",
        fdir = "../results/temp",
    shell:
        "mkdir -p {params.fdir} && cat {input.fa} >> {params.fas}"
        #"mkdir -p {params.fdir} && cat {input.fa} >> {params.fas} && echo '\n' >> {params.fas}"

rule clustalo:
    input:
        fa = "../results/consensus/all.fa",
    output:
        aln = "../results/consensus/all.aln.fa",
    params:
        wrap = "--wrap=80",
    shell:
        "clustalo --in={input.fa} {params.wrap} > {output.aln}"


#rule rehead_fasta:
#    input:
#        fa = "../results/consensus/{sample}.fa",
#    output:
#        fa = "../results/consensus/{sample}.reheaded.fa",
#		#fa = "../results/consensus/{sample}.fa",
#    params:
#        header = "{sample}"
#    log:
#        "../logs/consensus/{sample}_new_fa.log"
#    threads:
#        4
#    conda:
#        "../envs/bcf.yaml"    
#    shell:
#        "cat {input.fa} | python scripts/rehead.py {params.header} > {output.fa}"
#
