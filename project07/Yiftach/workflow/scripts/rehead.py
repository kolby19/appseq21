import os
import os.path as path
import sys

"""
give it a string for a header and fasta file.
alternatively it can read from stdin the fasta file.
outputs the new fast to stdout.
"""


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("error")
    elif len(sys.argv) == 2 or sys.argv[2] == "-":
        #x = sys.stdin.readline()
        #x = sys.stdin.readlines()
        #x = sys.stdin.read().splitlines()
        #lines = x[1:]
        sys.stdin.readline()
        lines = sys.stdin.readlines()
        header = sys.argv[1]
    else: #len(sys.argv) > 2:
        f = open(sys.argv[2], 'r')
        # discard the header
        f.readline()
        lines = f.readlines()
        header = sys.argv[1]

    s = [">" + header + "\n"] + lines
    #s = '\n'.join(s)
    s = ''.join(s)
    sys.stdout.writelines(s)
    #print(s)
    #print(sys.argv)

    

