import pandas as pd

df = pd.read_csv(snakemake.input.stats, sep='\t', names=["sequence", "length", "mapped", "unmapped"])

df["avg_depth"] = round(df["mapped"] / df["length"] * 1000, 2)

df.to_csv(snakemake.output.augmented_stats, sep='\t', index=False, header=False)