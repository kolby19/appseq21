rule bam_to_consensus:
    input:
        bam_sorted = "results/bam_sorted/{sample}.bam",
        bam_indexed = "results/bam_sorted/{sample}.bam.bai"
    output:
        fa = "results/consensus/{sample}.fa",
    log:
        "logs/consensus/{sample}_bam_to_consensus.log"
    params:
        out = "results/consensus/{sample}"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/bcf.yaml" 
    shell:
        "( samtools mpileup -A -Q 0 {input.bam_sorted} | ivar consensus -p {params.out} -m 3 -q 10 -i {wildcards.sample}) > {log} 2>&1"


rule combine_fastas:
    input:
        fastas = expand("results/consensus/{sample}.fa", sample=list(samples.index)),
    output:
        fa = "results/consensus/all.fa"
    conda:
        "../envs/bcf.yaml"   
    shell:
        "cat {input.fastas} > {output.fa}" 


rule clustalo:
    input:
        fa = "results/consensus/all.fa",
    output:
        aln = "results/phylogeny/all.aln.fa",
    log:
        "logs/phylogeny/clustalo.log"
    params:
        wrap = "--wrap=60",
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/bcf.yaml"
    shell:
        "clustalo --in={input.fa} --threads={threads} {params.wrap} -o {output.aln} > {log} 2>&1"