import pandas
import os
import pandas as pd
import sys


def write_sample_tsv_fastq(path, outdir):
    files = sorted(os.listdir(path))  # creates list with all files in path
    sample = sorted(list(set([f.split("_")[0] for f in files])))
    # df with first coloumn sample
    df = pd.DataFrame(sample, columns=["sample"])
    path_1 = [os.path.join(path, i) for i in files if "_1.fastq.gz" in i]
    path_2 = [os.path.join(path, i) for i in files if "_2.fastq.gz" in i]
    df["fq1"] = path_1
    df["fq2"] = path_2
    df.to_csv(outdir, index=False, sep="\t")

#path = "/mnt/c/Users/Max/box.fu/Master/AppliedSeqAnalysis/Snakemake/data/fastq/ncov"  # absolute path to the fastq files directory
#outdir = "samples.tsv"
#write_sample_tsv_fastq(path, outdir)

if __name__ == "__main__":
    path = sys.argv[1]
    outdir = sys.argv[2]
    write_sample_tsv_fastq(path, outdir)
