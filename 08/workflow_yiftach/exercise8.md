## sketch solution for part d
We can use a script (there are scripts available online) to process
the output of kraken 2. 
The idea is to exclude/include by taxonomy.
For example exclude everything human, or include everything in the
sars family.

I think this is what we should be using
https://github.com/jenniferlu717/KrakenTools
