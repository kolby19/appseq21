rule sam_to_bam:
    input:
        sam = "results/sam/{sample}.sam"
    output:
        bam = "results/bam/{sample}.bam"
    log:
        "logs/bam/{sample}_samtools_view.log"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/env.yaml"    
    shell:
        "samtools view -@ {threads} -S -b {input.sam} > {output.bam} 2> {log} 2>&1"


rule sort_bam:
    input:
        bam = "results/bam/{sample}.bam"
    output:
        bam_sorted = "results/bam_sorted/{sample}.bam"
    log:
        "logs/bam_sorted/{sample}_samtools_sort.log"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/env.yaml"   
    shell:
        "samtools sort -@ {threads} {input.bam} -o {output.bam_sorted} 2> {log} 2>&1"


rule index_bam:
    input:
        bam_sorted = "results/bam_sorted/{sample}.bam"
    output:
        bam_indexed = "results/bam_sorted/{sample}.bam.bai"
    log:
        "logs/bam_sorted/{sample}_samtools_index.log"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/env.yaml"    
    shell:
        "samtools index -@ {threads} {input.bam_sorted} > {output.bam_indexed} 2> {log} 2>&1"


rule bam_stats:
    input:
        bam_sorted = "results/bam_sorted/{sample}.bam",
        bam_indexed = "results/bam_sorted/{sample}.bam.bai"
    output:
        stats = "results/stats/{sample}.txt"
    log:
        "logs/stats/{sample}_samtools_idxstats.log"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/env.yaml"    
    shell:
        "samtools idxstats -@ {threads} {input.bam_sorted} > {output.stats} 2> {log} 2>&1"


rule average_depth:
    input:
        stats = "results/stats/{sample}.txt"
    output:
        augmented_stats = "results/augmented_stats/{sample}.txt"
    script:
        "../scripts/average_depth.py"