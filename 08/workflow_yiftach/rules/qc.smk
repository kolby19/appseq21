rule first_fastqc:
    input:
        fastq_1 = lambda wildcards: samples.at[wildcards.sample, "fq1"],
        fastq_2 = lambda wildcards: samples.at[wildcards.sample, "fq2"],
    output:
        report = touch("results/qc/raw_reports/{sample}.flag")
    log:
        "logs/qc/raw_reports/{sample}_fastqc.log"
    params:
        outdir = "results/qc/raw_reports"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/env.yaml"
    shell:
        "fastqc {input.fastq_1} {input.fastq_2} -t {threads} -o {params.outdir} > {log} 2>&1"


rule trimmomatic:
    input:
        fastq_1 = lambda wildcards: samples.at[wildcards.sample, "fq1"],
        fastq_2 = lambda wildcards: samples.at[wildcards.sample, "fq2"],
    output:
        fastq_1 = "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        unp_fastq_1 = "results/qc/trimmed_reads/unp_{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_reads/{sample}_2.fastq.gz",
        unp_fastq_2 = "results/qc/trimmed_reads/unp_{sample}_2.fastq.gz",
    log:
        "logs/qc/trimmed_reads/{sample}_trimmomatic.log"
    params:
        trimmer = config["trimmomatic"]["trimmer"]
    threads:
        4
    conda:
        "../envs/env.yaml"
    shell:
        """trimmomatic PE -threads {threads} {input.fastq_1} {input.fastq_2} \
        {output.fastq_1} {output.unp_fastq_1} {output.fastq_2} {output.unp_fastq_2} \
        {params.trimmer} > {log} 2>&1"""



rule second_fastqc:
    input:
        trimmed_fastq_1 = "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        trimmed_fastq_2 = "results/qc/trimmed_reads/{sample}_2.fastq.gz",       
    output:
        report_flag = touch("results/qc/trimmed_reports/{sample}.flag")
    log:
        "logs/qc/trimmed_reports/{sample}_fastqc.log"
    params:
        outdir = "results/qc/trimmed_reports"
    threads:
        workflow.cores * 0.75
    conda:
        "../envs/env.yaml"
    shell:
        "fastqc {input.trimmed_fastq_1} {input.trimmed_fastq_2} -t {threads} -o {params.outdir} > {log} 2>&1"


rule qualimap:
    input:
        bam_sorted = "results/bam_sorted/{sample}.bam",
        bam_indexed = "results/bam_sorted/{sample}.bam.bai"
    output:
        stats = "results/qc/mapping_stats/{sample}/qualimapReport.html"
    log:
        "logs/qc/mapping_stats/{sample}_qualimap.log"
    params:
        outdir = "results/qc/mapping_stats/{sample}"
    conda:
        "../envs/env.yaml"
    shell:
        "qualimap bamqc -bam {input.bam_sorted} -outformat HTML -outdir {params.outdir} > {log} 2>&1"


#rule multiqc:
#    input:
#        expand("results/qc/raw_reports/{sample}.flag", sample=list(samples.index)),
#        expand("results/qc/trimmed_reports/{sample}.flag", sample=list(samples.index)),
#        expand("results/qc/mapping_stats/{sample}/qualimapReport.html", sample=list(samples.index)),
#    output:
#        report = "results/qc/aggregated/multiqc_report.html"
#    log:
#        "logs/qc/aggregated/multiqc_report.log"
#    conda:
#        "../envs/env.yaml"
#    params:
#        searchdir = "results/qc",
#        outdir = "results/qc/aggregated"
#    shell:
#        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"


rule multiqc:
    input:
        expand("results/qc/raw_reports/{sample}.flag", sample=list(samples.index)),
        expand("results/qc/trimmed_reports/{sample}.flag", sample=list(samples.index)),
        expand("results/qc/kraken/{sample}.report", sample=list(samples.index)),
        expand("results/qc/mapping_stats/{sample}/qualimapReport.html", sample=list(samples.index)),
    output:
        report = "results/qc/aggregated/multiqc_report.html"
    log:
        "logs/qc/aggregated/multiqc_report.log"
    conda:
        "../envs/env.yaml"
    params:
        searchdir = "results/qc",
        outdir = "results/qc/aggregated"
    shell:
        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"






