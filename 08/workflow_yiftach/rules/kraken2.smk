if config["kraken2"]["active"]:
    rule kraken2:
        input:
            fastq_1 = lambda wildcards: samples.at[wildcards.sample, "fq1"] if config["kraken2"]["active"] else "",
            fastq_2 = lambda wildcards: samples.at[wildcards.sample, "fq2"] if config["kraken2"]["active"] else ""
        output:
            report = "results/qc/kraken/{sample}.report",
            out = "results/qc/kraken/{sample}.output"
        params:
            db = config["kraken2"]["db"]
        conda:
            "../envs/kraken2.yaml"
        threads:
            workflow.cores * 0.75
        shell:
            "kraken2 --threads {threads} --db {params.db} --gzip-compressed --paired {input.fastq_1} {input.fastq_2} --report {output.report} --output {output.out}"

