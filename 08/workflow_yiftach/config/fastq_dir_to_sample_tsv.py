import pandas
import os
import pandas as pd


def write_sample_tsv_fastq(path, outdir):
    files = sorted(os.listdir(path))  # creates list with all files in path
    sample = sorted(list(set([f.split("_")[0] for f in files])))
    # df with first coloumn sample
    df = pd.DataFrame(sample, columns=["sample"])
    path_1 = [os.path.join(path, i) for i in files if "_1.fastq.gz" in i]
    path_2 = [os.path.join(path, i) for i in files if "_2.fastq.gz" in i]
    df["fq1"] = path_1
    df["fq2"] = path_2
    df.to_csv(outdir, index=False, sep="\t")

path = "/storage/mi/bieniam98/ASA/data/ncov" # absolute path to the fastq files directory
outdir = "samples.tsv"
write_sample_tsv_fastq(path, outdir)
