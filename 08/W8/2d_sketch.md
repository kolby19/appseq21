## sketch solution for part d
We can use a script (there are scripts available online) to process the output of kraken 2. 
The idea is to exclude/include by taxonomy.
For example include all reads coming from the Sars family and exclude all other reads (unclassified, human, ...).
This can be done e.g. using this tool: https://github.com/jenniferlu717/KrakenTools