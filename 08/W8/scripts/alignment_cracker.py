import os
import sys
import pandas as pd
import numpy as np
import toolz
import scipy
import argh
import matplotlib.pyplot as plt

from scipy.stats import entropy
from toolz.curried import take, reduce, compose_left, curry
from toolz.curried import first, second

# reads the msa file in fasta format and creates a a dictionary
# key: sequence name value: sequence
def fastaToDict(fastapath):
    """
    read an MSA file in fasta format and create a dictionary,
    where key = name of the sequence (string) parsed from the header part
    value=the sequence (string)
    """
    if fastapath == sys.stdin:
        f = sys.stdin
    else:
        f = open(fastapath)
    names = []
    seqs = {}
    for line in f:
        if line[0] == ">":
            current = line[1:-1]
            names.append(current)
            seqs[current] = ""
        elif line[0] != "\n":
            seqs[current] += line[:-1]
    f.close()
    return seqs


def fastaToDF(fastapath):
    """input fastapath: path to a MSA in fasta format.
    output df: a data frame where each row represents a position and each column
    is a sequence of the MSA and the entries are the integer encoding of the
    nucleotide in that position.
    """
    seqs = fastaToDict(fastapath)
    names = [item for item in seqs]
    df = pd.DataFrame(columns=names)
    for name in names:
        df[name] = list(seqs[name])
    return df


def pointEntropy(df):
    """Input: a pandas dataFrame as the output of 'fastaToDF'.
    output: a vector of the entropy of each position.
    """
    alphabet2 = np.unique(df)
    mydict2 = dict(zip(alphabet2, range(len(alphabet2))))
    xs = df.applymap(mydict2.get).to_numpy().astype(int)
    ys = np.zeros(len(xs))
    for i in range(len(xs)):
        x = np.bincount(xs[i])
        ys[i] = entropy(x)
    return ys


def plotEntropy(xdf, outfile):
    """Input xdf: Entropy data file. The last two
    columns are the windowEntropy and the Ncount flag.
    imput outfile: path to save the figure.
    Will create a plot and save it as a png file.
    """
    names = xdf.columns
    n = len(xdf)
    pos = np.arange(n)
    y = xdf[names[-2]]
    Nflag = xdf[names[-1]]
    plt.plot(pos, y, Nflag)
    plt.yscale("log")
    plt.title("Entropy Window (log scale)")
    plt.xlabel("position")
    plt.ylabel("average entropy")
    plt.legend([names[-2], names[-1]])
    plt.savefig(outfile)
    plt.close()


def plotFromFile(infile, outfile):
    """Input: path to saved dataFrame which was the output of 'fastaToDF'.
    output: a vector of the entropy of each position.
    Will be saved in outfile.
    """
    df = pd.read_csv(infile, sep="\t", header=0)
    plotEntropy(df, outfile)


# def main(msa, width, outfile=sys.stdout):
def main(msa, width, outfile):
    """input msa: a path to multiple sequence alignment file,
    in fasta format.
    input width: Integer, the width of the sliding window.
    input outfile: where to save the output. default=stdout
    output: a data frame where the rows are positions of the sequences,
    the first columns are the sequences. The next to last column
    is the position-wise entropy, and the last column is the entropy
    on a sliding window.
    will save the df to outfile.
    If outfile is not '-', it will also save a plot as 'outfile.png'
    """
    df = fastaToDF(msa)
    ys = pointEntropy(df)
    hasN = (df.to_numpy() == "N").astype("int").sum(axis=1)
    # print(hasN, len(df.columns))
    nseqs = len(df.columns)
    win = int(width)
    n = len(ys)
    zs = np.zeros_like(ys)
    zs = np.cumsum(ys)
    for i in range(n - 1, win - 1, -1):
        zs[i] -= zs[i - win]
    zs[0 : win - 1] = 0
    zs /= win
    df["Entropy"] = ys
    df[str(width) + "windowEntropy"] = zs
    # df['Ncount'] = hasN / nseqs
    df["Ncount"] = hasN.astype("float") / float(width)
    # df['Ncount'] = hasN.astype('float') / width
    if outfile == "-":
        df.to_csv(sys.stdout, header=True, index=False, sep="\t")
    else:
        df.to_csv(outfile, header=True, index=False, sep="\t")
        #plotEntropy(df, outfile + ".png")


# assemble argument parser
parser = argh.ArghParser()
parser.add_commands([main, plotFromFile])


if __name__ == "__main__":
    """to read help: >python alignment_cacker.py help [main]"""
    parser.dispatch()
