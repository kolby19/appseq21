rule iqtree:
    input:
        aln = "results/phylogeny/all.aln.fa"
    output:
        tree = "results/phylogeny/all.aln.fa.treefile"
    log:
        "logs/phylogeny/iqtree.log"
    conda:
        "../envs/bcf.yaml"
    threads:
        workflow.cores * 0.75
    shell:
        "iqtree -s {input.aln} -T {threads} > {log} 2>&1"


rule plot_tree:
    input:
        tree = "results/phylogeny/all.aln.fa.treefile"
    output:
        pdf = "results/phylogeny/final_tree.pdf"
    log:
        "logs/phylogeny/iqtree.log"
    conda:
        "../envs/bcf.yaml"
    shell:
        "figtree -graphic PDF {input.tree} {output.pdf} > {log} 2>&1"


rule entropy:
    input:
        aln = "results/phylogeny/all.aln.fa",
    output:
        entropy = "results/similarity/entropy.csv",
    conda:
        "../envs/env2.yaml",
    params:
        win = config["window_size"],
    shell:
        "python scripts/alignment_cracker.py main {input.aln} {params.win} {output.entropy}"


rule entropy_plot:
    input:
        entropy = "results/similarity/entropy.csv",
    output:
        plot = "results/similarity/entropy.png",
    conda:
        "../envs/env2.yaml",
    shell:
        "python scripts/alignment_cracker.py plotFromFile {input.entropy} {output.plot}"

