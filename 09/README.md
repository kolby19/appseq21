# Worflow for sequence analysis of bacterial genomes

This is the final project for the course  "Applied sequence analysis" (FU
Berlin, Summer 2021).

## Instruction

The attached pdf (project's report) serves as the user guide to run and
configure the workflow.

This workflow runs on a Linux operating system and requires and installation of 
Conda (version 4.2 or higher) and Snakemake (version 6.5) (installable from Conda).

The minimal requirement to run this pipeline:
edit the file `workflow/config/config.yaml` and write in the path to '.tsv' file
which lists the sample names and paths for their reads.

There is a script `fastq_dir_to_sample_tsv.py` which can be used to create this
file.

To execute the workflow run `snakemake --cores [x] --use-conda` from the
`workflow` directory.
