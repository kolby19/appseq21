# Assignment 9: Bacterial genome analysis

## Project description

preQC: FastQC, Trimmomatic, (BBNorm), MultiQC

Assembly: Spades
Mapping: Bowtie2

postQC: Quast (Assembly), Qualimap (Mapping)

Annotation: Prokka

Core genome inferred based on Annotation: Roary

Phylogeny based on Core genome - Glue core genes for each species, then alignment. But tool exists

MLST: pubMLST (MST?)
ARG: CARD database

## config.yaml
denovo: True
mapping: False

outgroup: "../outgroup.fasta" # leave empty, then tree is unrooted

blacklist: "" # sample names to exclude from phylogeny
