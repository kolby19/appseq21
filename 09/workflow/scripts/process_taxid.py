import in_place
import time


if snakemake.params.taxid == "":
    
    print("Kraken2 reports can be found in results/qc/kraken2.")
    taxid = input("Enter taxonomy-id to be excluded from reads: ")
    
    with in_place.InPlace("config/config.yaml") as f:
        for line in f:
            if "taxid:" in line:
                line = line.replace("\"\"", taxid)
            f.write(line)

    print("Taxonomy-id got updated in config/config.yaml.")
