#################### De novo assembly ####################

rule spades_assembly:
    input:
        fastq_1 = "results/qc/trimmed_normalized_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_normalized_reads/{sample}_2.fastq.gz",
    output:
        fa = "results/assembly/spades/{sample}/{sample}.fa"
    log:
        "logs/assembly/spades/{sample}_spades_assembly.log"
    params:
        isolate = "--isolate" if config["spades"]["isolate"] else "",
        out = "results/assembly/spades/{sample}"
    threads:
        workflow.cores
    conda:
        "../envs/assembly.yaml"
    shell:
        "( spades.py -t {threads} -1 {input.fastq_1} -2 {input.fastq_2} {params.isolate} -o {params.out} && mv {params.out}/contigs.fasta {output.fa} ) > {log} 2>&1"


#################### Mapping assembly ####################

rule bowtie2_indexing:
    input:
        ref = config["bowtie2"]["reference"]
    output:
        index = multiext("results/index/ref_idx", ".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
    log:
        "logs/sam/bowtie_indexing.log"
    params:
        index_name = "results/index/ref_idx",
    conda:
        "../envs/assembly.yaml"
    shell:
        "bowtie2-build -f {input.ref} {params.index_name} > {log} 2>&1"


rule bowtie_mapping:
    input:
        fastq_1 = "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        #fastq_1 = "results/qc/trimmed_normalized_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_reads/{sample}_2.fastq.gz",
        #fastq_2 = "results/qc/trimmed_normalized_reads/{sample}_2.fastq.gz",
        index = multiext("results/index/ref_idx", ".1.bt2", ".2.bt2", ".3.bt2", ".4.bt2", ".rev.1.bt2", ".rev.2.bt2"),
    output:
        sam = temp("results/assembly/bowtie2/{sample}/{sample}.sam")
    log:
        "logs/assembly/bowtie2/{sample}_bowtie_mapping.log"
    params:
        index_name = "results/index/ref_idx"
    threads:
        workflow.cores
    conda:
        "../envs/assembly.yaml"
    shell:
        "bowtie2 -p {threads} -x {params.index_name} -1 {input.fastq_1} -2 {input.fastq_2} -S {output.sam} > {log} 2>&1"


rule sam_to_bam:
    input:
        sam = "results/assembly/bowtie2/{sample}/{sample}.sam"
    output:
        bam = temp("results/assembly/bowtie2/{sample}/{sample}.bam")
    log:
        "logs/bam/{sample}_samtools_view.log"
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/assembly.yaml"    
    shell:
        "samtools view -@ {threads} -S -b {input.sam} > {output.bam} 2> {log} 2>&1"


rule sort_bam:
    input:
        bam = "results/assembly/bowtie2/{sample}/{sample}.bam"
    output:
        bam_sorted = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam"
    log:
        "logs/bam_sorted/{sample}_samtools_sort.log"
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/assembly.yaml"   
    shell:
        "samtools sort -@ {threads} {input.bam} -o {output.bam_sorted} 2> {log} 2>&1"


rule index_bam:
    input:
        bam_sorted = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam"
    output:
        bam_indexed = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam.bai"
    log:
        "logs/bam_sorted/{sample}_samtools_index.log"
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/assembly.yaml"    
    shell:
        "samtools index -@ {threads} {input.bam_sorted} > {output.bam_indexed} 2> {log} 2>&1"


rule bam_to_consensus:
    input:
        bam_sorted = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam",
        bam_indexed = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam.bai"
    output:
        fa = "results/assembly/bowtie2/{sample}/{sample}.fa",
    log:
        "logs/consensus/{sample}_bam_to_consensus.log"
    params:
        out = "results/assembly/bowtie2/{sample}/{sample}"
    conda:
        "../envs/assembly.yaml" 
    shell:
        "( samtools mpileup -A -Q 0 {input.bam_sorted} | ivar consensus -p {params.out} -m 5 -q 10 -i {wildcards.sample} ) > {log} 2>&1"