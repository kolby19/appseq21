rule prokka_annotation:
    input:
        fa = lambda wildcards: "results/assembly/{assembler}/{sample}/{sample}.fa",
    output:
        gff = "results/annotation/{assembler}/{sample}/{sample}.gff",
    log:
        "logs/annotation/{assembler}/{sample}_prokka_annotation.log"
    params:
        species = ( "--species " + config["prokka"]["species"] ) if config["prokka"]["species"] else "",
        proteins = ( "--proteins " + config["prokka"]["proteins"] ) if config["prokka"]["proteins"] else "",
        outdir = "results/annotation/{assembler}/{sample}/",
        prefix = "{sample}"
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/annotation.yaml"
    shell:
        "prokka --cpus {threads} {params.species} {params.proteins} --outdir {params.outdir} --prefix {params.prefix} {input.fa} --force > {log} 2>&1"


rule prokka_annotation_outgroup:
    input:
        fa = config["phylogeny"]["outgroup"],
    output:
        gff = "results/annotation/outgroup/outgroup.gff",
    log:
        "logs/annotation/outgroup/outgroup_prokka_annotation.log"
    params:
        species = ( "--species " + config["prokka"]["species"] ) if config["prokka"]["species"] else "",
        proteins = ( "--proteins " + config["prokka"]["proteins"] ) if config["prokka"]["proteins"] else "",
        outdir = "results/annotation/outgroup/",
        prefix = "outgroup",
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/annotation.yaml"
    shell:
        "prokka --cpus {threads} {params.species} {params.proteins} --outdir {params.outdir} --prefix {params.prefix} {input.fa} --force > {log} 2>&1"
