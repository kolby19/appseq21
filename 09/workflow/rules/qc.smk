rule first_fastqc_1:
    input:
        lambda wildcards: samples.at[wildcards.sample, "fq1"] if wildcards.sample in samples.index else ''
    output:
        html="results/qc/first_fastqc/1/{sample}_1_fastqc.html",
        zip="results/qc/first_fastqc/1/{sample}/{sample}_1_fastqc.zip"
    threads:
        min(workflow.cores, 4)
    wrapper:
        "0.66.0/bio/fastqc"


rule first_fastqc_2:
    input:
        lambda wildcards: samples.at[wildcards.sample, "fq2"] if wildcards.sample in samples.index else ''
    output:
        html="results/qc/first_fastqc/2/{sample}_2_fastqc.html",
        zip="results/qc/first_fastqc/2/{sample}/{sample}_2_fastqc.zip"
    threads:
        min(workflow.cores, 4)
    wrapper:
        "0.66.0/bio/fastqc"


rule trimmomatic:
    input:
        fastq_1 = lambda wildcards: samples.at[wildcards.sample, "fq1"] if wildcards.sample in samples.index else '',
        fastq_2 = lambda wildcards: samples.at[wildcards.sample, "fq2"] if wildcards.sample in samples.index else '',
    output:
        fastq_1 = "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        unp_fastq_1 = "results/qc/trimmed_reads/unp_{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_reads/{sample}_2.fastq.gz",
        unp_fastq_2 = "results/qc/trimmed_reads/unp_{sample}_2.fastq.gz",
    log:
        "logs/qc/trimmed_reads/{sample}_trimmomatic.log"
    params:
        trimmer = config["trimmomatic"]["trimmer"]
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/qc.yaml"
    shell:
        """trimmomatic PE -threads {threads} {input.fastq_1} {input.fastq_2} \
        {output.fastq_1} {output.unp_fastq_1} {output.fastq_2} {output.unp_fastq_2} \
        {params.trimmer} > {log} 2>&1"""


rule kraken2:
    input:
        fastq_1 = "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_reads/{sample}_2.fastq.gz"
    output:
        kraken = "results/qc/kraken2/{sample}.kraken2",
        report = "results/qc/kraken2/{sample}.report"
    log:
        "logs/qc/kraken2/{sample}.log"
    params:
        db = config["kraken2"]["db"]
    conda:
        "../envs/qc.yaml"
    threads:
        min(workflow.cores, 4)
    shell:
        "kraken2 --threads {threads} --db {params.db} --gzip-compressed --paired {input.fastq_1} {input.fastq_2} --report {output.report} --output {output.kraken} > {log} 2>&1"


rule kraken2_qc: # use "snakemake ... --until kraken2_qc" to only execute this rule (and its dependencies)
    input:
        expand("results/qc/kraken2/{sample}.{ext}", sample=list(samples.index), ext=["kraken2", "report"]),
    output:
        report = "results/qc/aggregated/kraken2/multiqc_report.html"
    log:
        "logs/qc/aggregated/kraken2/multiqc_report.log"
    conda:
        "../envs/qc.yaml"
    params:
        searchdir = "results/qc/kraken2",
        outdir = "results/qc/aggregated/kraken2"
    shell:
        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"


rule kraken2_cleaning:
    input:
        #flag = "results/qc/kraken2/taxid.flag",
        kraken = "results/qc/kraken2/{sample}.kraken2",
        report = "results/qc/kraken2/{sample}.report",
        fastq_1 = "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_reads/{sample}_2.fastq.gz"
    output:
        fastq_1 = "results/qc/trimmed_cleaned_reads/{sample}_1.fastq",
        fastq_2 = "results/qc/trimmed_cleaned_reads/{sample}_2.fastq"
    log:
        "logs/qc/kraken2/{sample}_cleaning.log"
    params:
        taxid = str(config["kraken2"]["taxid"]),
        exclude = "--exlude" if config["kraken2"]["exclude"] else "",
        include_children = "--include-children" if config["kraken2"]["include_children"] else "",
        include_parents = "--include-parents" if config["kraken2"]["include_parents"] else "",
    conda:
        "../envs/qc.yaml"
    threads:
        min(workflow.cores, 4)
    shell:
        """
        extract_kraken_reads.py -k {input.kraken} -s1 {input.fastq_1} -s2 {input.fastq_2} \
        -o {output.fastq_1} -o2 {output.fastq_2} --fastq-output --report {input.report} \
        -t {params.taxid} {params.exclude} {params.include_children} {params.include_parents} \
        > {log} 2>&1
        """


rule bbnorm:
    input:
        fastq_1 = "results/qc/trimmed_cleaned_reads/{sample}_1.fastq" if config["kraken2"]["active"] else "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_cleaned_reads/{sample}_2.fastq" if config["kraken2"]["active"] else "results/qc/trimmed_reads/{sample}_2.fastq.gz",
    output:
        fastq_1 = "results/qc/trimmed_normalized_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_normalized_reads/{sample}_2.fastq.gz",
    log:
        "logs/qc/trimmed_reads/{sample}_bbnorm.log"
    params:
        target = config["bbnorm"]["target"],
        min = config["bbnorm"]["min"],
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/qc.yaml"
    shell:
        """
        bbnorm.sh t={threads} in1={input.fastq_1} in2={input.fastq_2} \
        out1={output.fastq_1} out2={output.fastq_2} target={params.target} min={params.min} \
        > {log} 2>&1
        """


rule second_fastqc_1:
    input:
        "results/qc/trimmed_normalized_reads/{sample}_1.fastq.gz"
    output:
        html="results/qc/second_fastqc/1/{sample}_1_fastqc.html",
        zip="results/qc/second_fastqc/1/{sample}/{sample}_1_fastqc.zip"
    threads:
        min(workflow.cores, 4)
    wrapper:
        "0.66.0/bio/fastqc"


rule second_fastqc_2:
    input:
        "results/qc/trimmed_normalized_reads/{sample}_2.fastq.gz"
    output:
        html="results/qc/second_fastqc/2/{sample}_2_fastqc.html",
        zip="results/qc/second_fastqc/2/{sample}/{sample}_2_fastqc.zip"
    threads:
        min(workflow.cores, 4)
    wrapper:
        "0.66.0/bio/fastqc"


rule first_multiqc:
    input:
        expand("results/qc/first_fastqc/{r}/{sample}_{r}_fastqc.html", sample=list(samples.index), r=["1", "2"]),
    output:
        report = "results/qc/aggregated/first/multiqc_report.html"
    log:
        "logs/qc/aggregated/first/multiqc_report.log"
    conda:
        "../envs/qc.yaml"
    params:
        searchdir = "results/qc/first_fastqc",
        outdir = "results/qc/aggregated/first"
    shell:
        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"


rule second_multiqc:
    input:
        expand("results/qc/second_fastqc/{r}/{sample}_{r}_fastqc.html", sample=list(samples.index), r=["1", "2"]),
    output:
        report = "results/qc/aggregated/second/multiqc_report.html"
    log:
        "logs/qc/aggregated/second/multiqc_report.log"
    conda:
        "../envs/qc.yaml"
    params:
        searchdir = "results/qc/second_fastqc" + ( " results/qc/kraken2" if config["kraken2"]["active"] else "" ),
        outdir = "results/qc/aggregated/second"
    shell:
        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"
