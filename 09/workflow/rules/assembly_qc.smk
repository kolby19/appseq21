rule quast:
    input:
        fastq_1 = "results/qc/trimmed_normalized_reads/{sample}_1.fastq.gz" if config["kraken2"]["active"] and "{assembler}" == "spades" else "results/qc/trimmed_reads/{sample}_1.fastq.gz",
        fastq_2 = "results/qc/trimmed_normalized_reads/{sample}_2.fastq.gz" if config["kraken2"]["active"] and "{assembler}" == "spades" else "results/qc/trimmed_reads/{sample}_2.fastq.gz",
        fa = "results/assembly/{assembler}/{sample}/{sample}.fa"
    output:
        report = "results/qc/assembly_stats/quast/{assembler}/{sample}/report.txt"
    log:
        "logs/qc/assembly_stats/quast/{assembler}/{sample}_quast.log"
    params:
        out = "results/qc/assembly_stats/quast/{assembler}/{sample}",
        ref = ( "-r " + config["bowtie2"]["reference"] ) if config["bowtie2"]["reference"] else ""
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/assembly_qc.yaml"
    shell:
        "quast.py -t {threads} {input.fa} -o {params.out} -1 {input.fastq_1} -2 {input.fastq_2} {params.ref} > {log} 2>&1"


rule idxstats:
    input:
        bam_sorted = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam",
        bam_indexed = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam.bai"
    output:
        stats = "results/qc/assembly_stats/idxstats/{sample}.idxstats"
    log:
        "logs/stats/{sample}_samtools_idxstats.log"
    threads:
        min(workflow.cores, 4)
    conda:
        "../envs/assembly.yaml"    
    shell:
        "samtools idxstats -@ {threads} {input.bam_sorted} > {output.stats} 2> {log} 2>&1"


rule qualimap:
    input:
        bam_sorted = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam",
        bam_indexed = "results/assembly/bowtie2/{sample}/{sample}_sorted.bam.bai"
    output:
        stats = "results/qc/assembly_stats/bamqc/{sample}/qualimapReport.html"
    log:
        "logs/qc/assembly_stats/bamqc/{sample}_qualimap.log"
    params:
        outdir = "results/qc/assembly_stats/bamqc/{sample}"
    conda:
        "../envs/assembly_qc.yaml"
    shell:
        "qualimap bamqc -bam {input.bam_sorted} -outformat HTML -outdir {params.outdir} > {log} 2>&1"


rule spades_assembly_multiqc:
    input:
        expand("results/qc/assembly_stats/quast/spades/{sample}/report.txt", sample=list(samples.index)) if config["spades"]["active"] else [],
    output:
        report = "results/qc/aggregated/assembly/spades/multiqc_report.html"
    log:
        "logs/qc/aggregated/assembly/spades/multiqc_report.log"
    conda:
        "../envs/qc.yaml"
    params:
        searchdir = "results/qc/assembly_stats/quast/spades",
        outdir = "results/qc/aggregated/assembly/spades"
    shell:
        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"


rule bowtie2_assembly_multiqc:
    input:
        expand("results/qc/assembly_stats/quast/bowtie2/{sample}/report.txt", sample=list(samples.index)) if config["spades"]["active"] else [],
        expand("results/qc/assembly_stats/idxstats/{sample}.idxstats", sample=list(samples.index)) if config["bowtie2"]["active"] else [],
        expand("results/qc/assembly_stats/bamqc/{sample}/qualimapReport.html", sample=list(samples.index)) if config["bowtie2"]["active"] else [],
    output:
        report = "results/qc/aggregated/assembly/bowtie2/multiqc_report.html"
    log:
        "logs/qc/aggregated/assembly/bowtie2/multiqc_report.log"
    conda:
        "../envs/qc.yaml"
    params:
        searchdir = "results/qc/assembly_stats/quast/bowtie2 results/qc/assembly_stats/idxstats results/qc/assembly_stats/bamqc",
        outdir = "results/qc/aggregated/assembly/bowtie2"
    shell:
        "multiqc {params.searchdir} --force -o {params.outdir} > {log} 2>&1"