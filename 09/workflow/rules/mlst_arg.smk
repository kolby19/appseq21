#################### Perform Multilocus sequence typing ####################

rule mlst:
    input:
        expand("results/assembly/{assembler}/{sample}/{sample}.fa", assembler=assembler(), sample=list(samples.index))
    output:
        "results/mlst/mlst.csv"
    log:
        "logs/mlst/mlst.log"
    conda:
        "../envs/mlst_arg.yaml"
    shell:
        "( mlst --csv {input} > {output} ) > {log} 2>&1"


#################### Find Antibiotic resistance genes ####################

rule arg:
    input:
        fa = lambda wildcards: "results/assembly/"+wildcards.assembler+"/"+wildcards.sample+"/"+wildcards.sample+".fa",
    output:
        tsv = "results/arg/{assembler}/{sample}_arg.json"
    log:
        "logs/arg/{assembler}/{sample}_arg.log"
    params:
        load_db = "rgi load --card_json {} &&".format(config["arg"]["db"]) if config["arg"]["db"] else "",
        out = "results/arg/{assembler}/{sample}_arg"
    conda:
        "../envs/mlst_arg.yaml"
    threads:
        workflow.cores
    shell:
        """( {params.load_db} \
        rgi main -n {threads} --input_sequence {input.fa} --output_file {params.out} \
        --input_type contig --include_loose --clean ) > {log} 2>&1
        """


rule arg_heatmap:
    input:
        jsons = expand("results/arg/{assembler}/{sample}_arg.json", assembler=assembler(), sample=list(samples.index))
    output:
        png = "results/arg/{assembler}/arg_heatmap.png"
    log:
        "logs/arg/{assembler}/arg_heatmap.log"
    params:
        jsons = "results/arg/{assembler}"
    conda:
        "../envs/mlst_arg.yaml"
    shell:
        "( rgi heatmap -i {params.jsons} -o {output.png} && mv {params.jsons}/*.png {output.png} ) > {log} 2>&1"
