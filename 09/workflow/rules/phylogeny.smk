rule roary_fix: # Fixes "Use of uninitialized value in require at .../Encode.pm line 61." error
    output:
        flag = touch("results/annotation/temp/flag.done")
    log:
        "logs/annotation/temp/enc2xs_fix.log"
    conda:
        "../envs/phylogeny.yaml"
    shell:
        "enc2xs -C > {log} 2>&1"


def get_whitelisted_samples():
    return list(set(list(samples.index)) - set(config["phylogeny"]["blacklist"]))


rule roary_core_genome_and_alignment:
    input:
        gffs = expand("results/annotation/{assembler}/{sample}/{sample}.gff", sample=get_whitelisted_samples(), assembler="{assembler}"),
        outgroup_gff = "results/annotation/outgroup/outgroup.gff" if config["phylogeny"]["outgroup"] else [],
        flag = "results/annotation/temp/flag.done"
    output:
        aln = "results/phylogeny/{assembler}/roary/core_gene_alignment.aln"
    log:
        "logs/phylogeny/{assembler}/roary.log"
    params:
        gffs = "results/annotation/{assembler}/*/*.gff" + (" results/annotation/outgroup/*.gff" if config["phylogeny"]["outgroup"] else ""),
        out = "results/phylogeny/{assembler}/roary/"
    threads:
        workflow.cores
    conda:
        "../envs/phylogeny.yaml"
    shell:
        "( roary -e -mafft -p {threads} -f {params.out} {input.gffs} {input.outgroup_gff} && mv -f {params.out}*/core_gene_alignment.aln {params.out}/core_gene_alignment.aln ) > {log} 2>&1"


rule raxml_phylogeny:
    input:
        aln = "results/phylogeny/{assembler}/roary/core_gene_alignment.aln"
    output:
        tree = "results/phylogeny/{assembler}/roary/{assembler}.raxml.bestTree"
    log:
        "logs/phylogeny/{assembler}/raxml.log"
    params:
        outdir = "results/phylogeny/{assembler}/roary/{assembler}",
        outgroup = "--outgroup outgroup" if config["phylogeny"]["outgroup"] else ""
    conda:
        "../envs/phylogeny.yaml"
    threads:
        workflow.cores
    shell:
        """
        raxml-ng --threads {threads} --msa {input.aln} {params.outgroup} --all \
        --msa-format FASTA --bs-trees 200 --model GTR+G --prefix {params.outdir} \
        > {log} 2>&1
        """


rule plot_tree:
    input:
        tree = "results/phylogeny/{assembler}/roary/{assembler}.raxml.bestTree"
    output:
        pdf = "results/phylogeny/{assembler}_final_tree.pdf"
    log:
        "logs/phylogeny/{assembler}/figtree.log"
    conda:
        "../envs/phylogeny.yaml"
    shell:
        "figtree -graphic PDF {input.tree} {output.pdf} > {log} 2>&1"
